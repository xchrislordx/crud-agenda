from flask import request

# Módulos propios
from models import *
from flask_restx import Resource

class ContactoController(Resource):
    def get(self, id):
        contact = Contacts.query.get_or_404(id)
        return contactSchema.dump(contact)

class ContactoPostController(Resource):
    def get(self):
        contacts = Contacts.query.all()
        #contacts = Contacts.query.filter(Contacts.name.ilike("ala%")).all()
        return contactsSchema.dump(contacts)

    def post(self):
        data = request.get_json()

        new_contact = Contacts(
            name=data['name'],
            lastname=data['lastname'],
            movil=data['movil'],
            email=data['email']
        )

        db.session.add(new_contact)
        db.session.commit()
        return contactSchema.dump(new_contact)