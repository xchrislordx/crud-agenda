
# Módulos Propios
from server import *
from models import *

from controllers.contactoController import ContactoController, ContactoPostController

contacto = api.namespace('api', description='Contacto API')
contacto.add_resource(ContactoPostController, '/contact')
contacto.add_resource(ContactoController, '/contact/<int:id>')

if __name__ == "__main__":
	app.run(port=config['PORT'], debug=config['DEBUG'])
