from . import db, ma

# https://docs.sqlalchemy.org/en/13/core/type_basics.html
class Contacts(db.Model):
    id          = db.Column(db.Integer, primary_key=True)
    name        = db.Column(db.String(50), unique=False, nullable=False)
    lastname    = db.Column(db.String(50), unique=False, nullable=False)
    movil       = db.Column(db.String(15), unique=True, nullable=False)
    email       = db.Column(db.String(90), unique=True, nullable=False)
    activo      = db.Column(db.Boolean, unique=False, nullable=False, default=1)

class ContactSchema(ma.Schema):
    class Meta:
        fields = ("id", "name", "lastname", "movil", "email", "estado")

contactSchema = ContactSchema()
contactsSchema = ContactSchema(many=True)