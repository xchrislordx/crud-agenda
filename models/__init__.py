# Librerías para SQLAlchemy
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow

# Módulos propios
from server import app
from database.connection import dbConnection
app.config['SQLALCHEMY_DATABASE_URI'] = dbConnection["URI"]
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = dbConnection["TRACK_MODIFICATIONS"]
app.config["SQLALCHEMY_ECHO"] = dbConnection["ECHO"]

db = SQLAlchemy(app)
ma = Marshmallow(app)

# Importar Modelos
from models.contactModel import *

db.create_all()